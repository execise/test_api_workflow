import boto3
import importlib
from flask import Flask, request, jsonify, abort
import app_db as db
import test_setting as test_setting

from tinydb import TinyDB, Query
import logging
import time
import json

# logging.basicConfig(filename="app.log", level=logging.DEBUG)
logger = logging.getLogger("generation workflow test")

# AWS_ACCESS_KEY_ID = "AKIA4AQEYJL3IX7UWHIY"
# AWS_SECRET_ACCESS_KEY = "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"

# BUCKET_NAME = "generate-1"

AWS_ACCESS_KEY_ID = test_setting.AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY = test_setting.AWS_SECRET_ACCESS_KEY
BUCKET_NAME = test_setting.BUCKET_NAME

app = Flask(__name__)


s3_client = boto3.client(
    "s3",
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
)

file_db = TinyDB('file_db.json')
# catagory_db = TinyDB('catagory.json')
# product_db = TinyDB('product.json')

catagory_table = file_db.table('catagory_table')
product_table = file_db.table('product_table')

# asset_png = 'https://www.wikihow.com/Get-the-URL-for-Pictures#/Image:Get-the-URL-for-Pictures-Step-1-Version-6.jpg'
# asset_url = 'https://simple-books-api.glitch.me/books/1'

asset_png = test_setting.asset_png
asset_url = test_setting.asset_url

assets = [asset_png, asset_url]


def upload_to_s3(file_path, bucket_name, file_name):
    try:
        s3_client.upload_file(file_path, bucket_name, file_name)
        logger.info(f"{file_name} upload successfully")
        return file_name

    except Exception as e:
        logger.error("Error:", e)
        return


def add_record_into_catagory():
    # query = Query()
    all_records = catagory_table.all()
    if len(all_records) == 0:
        new_id = 1000
    else:
        new_id = max(all_records, key=lambda x: x['id'])['id'] + 1    
    file_info = {"id": new_id, "status": "ingesting"}
    logger.info(f"file_info : {file_info}")
    catagory_table.insert(file_info)
    return file_info


def add_record_into_product(input_id):
    # query = Query()
    all_records = product_table.all()
    if len(all_records) == 0:
        new_id = 10000
    else:
        new_id = max(all_records, key=lambda x: x['id'])['id'] + 1    
    pruduct_info = {"id": new_id, 
                    "input_id": input_id,
                    "status": "processing"}
    logger.info(f"pruduct_info : {pruduct_info}")
    product_table.insert(pruduct_info)
    return pruduct_info
         

def generate_catagory():
    # DEBUG-FIX 
    # true : generate ok 
    # false: generate false
    # return False
    return True

def generate_product(input_id):
    # DEBUG-FIX
    # true : generate ok 
    # false: generate false
    # return False
    return True

def update_file_db_catagory(catagory_id, catagory_result):
    if not catagory_result:
        catagory_table.update({'status': 'failed'}, Query().id == catagory_id)
    else:
        catagory_table.update({'status': 'complete', 'assets' : assets}, Query().id == catagory_id)
    
    return 


def update_file_db_product(product_id, product_result):
    if not product_result:
        product_table.update({'status': 'failed'}, Query().id == product_id)
    else:
        product_table.update({'status': 'complete', 'assets' : assets}, Query().id == product_id)
    
    return 

# Define a Flask endpoint to receive file uploads
@app.route("/upload", methods=["POST"])
def upload_file():
    if not request.json or not "file_path" in request.json:
        abort(400)

    file_name = request.json["file_name"]
    file_path = request.json["file_path"]
    
    if not "work_flow_level" in request.json:
        work_flow_level = 5
    else:
        work_flow_level = request.json["work_flow_level"]
        
    logger.info(f'work_flow_level : {work_flow_level}')
        
    request.json["file_path"]
    bucket_name = BUCKET_NAME

    try:
        logger.info(f'start upload')
        upload_file_name = upload_to_s3(file_path, bucket_name, file_name)
    except:
        logger.error(f'upload error')
        return jsonify({"bad upload data"}), 400

    file_info = add_record_into_catagory()
    
    # DEBUG-FIX 
    catagory_result = generate_catagory()
        
    # DEBUG-FIX
    # time.sleep(100)
    time.sleep(10)

    if work_flow_level == test_setting.work_flow_level['catagory_not_new_record']:
        return jsonify({"file_name": file_name}), 201

    if work_flow_level == test_setting.work_flow_level['catagory_upd_timeout']:
        time.sleep(180)
        
    # DEBUG-FIX     
    if work_flow_level > test_setting.work_flow_level['catagory_fail']:
        catagory_result = True        
    else:
        catagory_result = False
    update_file_db_catagory(file_info['id'], catagory_result)

    # DEBUG-FIX
    # time.sleep(100)
    time.sleep(10) 

    # DEBUG-FIX

    if catagory_result:
        if work_flow_level == test_setting.work_flow_level['catagory_complete_product_not_new_record']:
            return jsonify({"file_name": file_name}), 201

        product_info = add_record_into_product(file_info['id'])
        product_result = generate_product(file_info['id'])

        # time.sleep(100)
        time.sleep(10) 

        if work_flow_level == test_setting.work_flow_level['product_processing']:
            return jsonify({"file_name": file_name}), 201

        if work_flow_level == test_setting.work_flow_level['product_upd_timeout']:
            time.sleep(180)

        # DEBUG-FIX 
        if work_flow_level > test_setting.work_flow_level['product_fail']:
            product_result = True        
        else:
            product_result = False
        update_file_db_product(product_info['id'], product_result)


    return jsonify({"file_name": file_name}), 201

@app.route('/input', methods=['GET'])
def get_catagory():
    catagory_records = catagory_table.all()

    return jsonify(catagory_records)

@app.route('/product', methods=['GET'])
def get_product():
    product_records = product_table.all()

    return jsonify(product_records)

@app.route('/change', methods=['PUT'])
def change_catagory_product():
    if not request.json:
        abort(400)
    new_record = json.loads(request.json)

    if 'table' not in new_record:
        abort(400)
    if new_record['table'] not in ['catagory', 'product']:
        abort(400)
    if 'new_data' not in new_record:
        abort(400)
    new_data = new_record['new_data']
    if 'id' not in new_data:
        abort(400)

    query = Query()
    if new_record['table'] == 'catagory':
        if catagory_table.contains(query.id == new_data['id']):
            catagory_table.update(new_data, query.id == new_data['id'])
        else:
            catagory_table.insert(new_data)
    else:
        #new_record['table'] == "product":
        if product_table.contains(query.id == new_data['id']):
            product_table.update(new_data, query.id == new_data['id'])
        else:
            product_table.insert(new_data)

    file_db.close()
    return jsonify(new_record)


if __name__ == "__main__":
    app.run(debug=True)
