import sys

sys.path.append("..")
sys.path.append(".")

import pytest
import os
import logging
import time
import signal
import requests
# from tinydb import TinyDB, Query

from msg_get import *
from msg_upload import *

import test_setting as test_setting
# import multiprocessing
import threading

logger = logging.getLogger("generation workflow test")

def timeout_handler(signum, frame):
    raise TimeoutError("Execution took too long")

# Set a timeout of 3 minutes
signal.signal(signal.SIGALRM, timeout_handler)
# signal.alarm(180) # 3 minutes in seconds
signal.alarm(100) # 3 minutes in seconds


def upload_file(req_upload, upload_data):
    logger.info(f"upload_data: {upload_data}")


    r = req_upload.opd_post(url_add='/upload', data=upload_data)
    if r.status_code == 201:
        logger.info("File uploaded successfully.")
    else:
        logger.error("Failed to upload file:", r.status_code)
        logger.error(r.content)
        assert r.status_code == 201
    return r

def process_upload_file(req_upload, old_file_name, old_file_path, new_file_name, work_flow_level):
    logger.info(f'old_file_name : {old_file_name}')
    logger.info(f'new_file_name : {new_file_name}')    
    file_path = old_file_path + '/' + old_file_name
    logger.info(f"file_path : {file_path}")

    if not os.path.isfile(file_path):
        logger.error(f"upload file path invalid")
        assert False

    if not isinstance(file_path, str):
        logger.error(f"upload file path not str type")
        assert False

    if not isinstance(new_file_name, str):
        logger.error(f"upload new file name invalid")
        assert False

    if len(new_file_name.strip()) == 0:
        logger.error(f"upload new file name invalid")
        assert False

    upload_data = {}
    upload_data["file_path"] = file_path
    upload_data["file_name"] = new_file_name
    upload_data["work_flow_level"] = work_flow_level


    logger.info(f"upload_data : {upload_data}")    

    upload_process = threading.Thread(target=upload_file, args=(req_upload,upload_data))
    # upload_process = multiprocessing.Process(target=upload_file, args=(req_upload,upload_data))
    upload_process.start()
    return

    
def get_catagory_records(req_get):
    r = req_get.opd_get_catagory()
    
    logger.info(f'r.status_code : {r.status_code}')
    assert r.status_code == 200, "get catagory record error"

    catagory_records = json.loads(r.content)
    logger.info(f'catagory_records : {catagory_records}')
    return catagory_records

def get_product_records(req_get):
    r = req_get.opd_get_product()
    logger.info(f'r.status_code : {r.status_code}')
    assert r.status_code == 200, "get product record error"
    
    product_records = json.loads(r.content)
    logger.info(f'product_records : {product_records}')
    return product_records
    
def find_new_catagory_record(req_get, old_catagory_records):
    time_count = 0
    while time_count < 20:
        ing_catagory_records = get_catagory_records(req_get)
        logger.info(f'ing_catagory_records : {ing_catagory_records}')

        old_catagory_lenght = len(old_catagory_records)
        new_catagory_length = len(ing_catagory_records)

        logger.info(f'old_catagory_lenght : {old_catagory_lenght}')
        logger.info(f'new_catagory_length : {new_catagory_length}')
        if new_catagory_length - old_catagory_lenght == 1:
            break
        time.sleep(10)
        time_count += 1
        
    assert new_catagory_length - old_catagory_lenght == 1, "catagory not add one new record"
        
    for new_record in ing_catagory_records:
        if 'id' not in new_record.keys():
            logger.error(f"catagory new record field missing : {new_record}")
            assert False
        if 'status' not in new_record.keys():
            logger.error(f"catagory new record field missing : {new_record}")
            assert False
        if new_record['id'] not in [old_record['id'] for old_record in old_catagory_records]:
            insert_catagory_record = new_record
            break
    return insert_catagory_record


def find_new_product_record(req_get, old_product_records):
    time_count = 0
    while time_count < 20:
        ing_product_records = get_product_records(req_get)
        logger.info(f'ing_product_records : {ing_product_records}')

        old_product_lenght = len(old_product_records)
        new_product_length = len(ing_product_records)

        logger.info(f'old_product_lenght : {old_product_lenght}')
        logger.info(f'new_product_length : {new_product_length}')
        if new_product_length - old_product_lenght == 1:
            break
        time.sleep(10)
        time_count += 1

    assert new_product_length - old_product_lenght == 1, "product not add one new record"

    for new_record in ing_product_records:
        if 'id' not in new_record:
            logger.error(f"product new record field missing : {new_record}")
            assert False
        if 'input_id' not in new_record:
            logger.error(f"product new record field missing : {new_record}")
            assert False
        if 'status' not in new_record:
            logger.error(f"product new record field missing : {new_record}")
            assert False
        if new_record['id'] not in [old_record['id'] for old_record in old_product_records]:
            ing_product_records = new_record
            break
    return ing_product_records


def find_upd_catagory_record(req_get, insert_new_catagory_id):
    time_count = 0
    while time_count < 20:
        catagory_records = get_catagory_records(req_get)
        for catagory_record in catagory_records:
            if catagory_record['id'] == insert_new_catagory_id:
                logger.info(f"catagory_record['id'] : {catagory_record['id']}")
                logger.info(f"insert_new_catagory_id : {insert_new_catagory_id}")
                logger.info(f"catagory_record['status'] : {catagory_record['status']}")
                if catagory_record['status'] != 'ingesting':
                    logger.info(f'catagory status change : {catagory_record}')                    
                    return catagory_record
                    
        time.sleep(10)
        time_count += 1


def find_upd_product_record(req_get, insert_new_product_id):
    time_count = 0
    while time_count < 20:
        product_records = get_product_records(req_get)
        for product_record in product_records:
            if product_record['id'] == insert_new_product_id:
                logger.info(f"product_record['id'] : {product_record['id']}")
                logger.info(f"insert_new_product_id : {insert_new_product_id}")
                logger.info(f"product_record['status'] : {product_record['status']}")
                if product_record['status'] != 'processing':
                    logger.info(f'product status change : {product_record}')
                    return product_record

        time.sleep(10)
        time_count += 1


def check_asset_valid(assets):
    logger.info(f"assets : {assets}")
    if len(assets) == 0:
        logger.error(f"assets is empty")
        assert False

    for asset_url in assets:
        payload = {}
        headers = {}

        response = requests.request("GET", asset_url, headers=headers, data=payload)
        assert response.status_code == 200, "get assert record error"
    return


def process_workflow(old_file_name, old_file_path,
    new_file_name,
    work_flow_level, test_type,
    req_get, req_upload
                     ):

    logger.info(f'upload old_file_name : {old_file_name}')
    logger.info(f'upload old_file_path : {old_file_path}')
    logger.info(f'upload new_file_name : {new_file_name}')
    logger.info(f'generate process work_flow_level : {work_flow_level}')

    actual_result = {}
    old_catagory_records = get_catagory_records(req_get)
    old_product_records = get_product_records(req_get)
    logger.info(f"old_product_records : {old_product_records}")

    try:
        process_upload_file(req_upload, old_file_name, old_file_path,new_file_name, work_flow_level)

        insert_new_catagory_record = find_new_catagory_record(req_get, old_catagory_records)
        if insert_new_catagory_record[
                   'status'] != 'ingesting':
            actual_result['check 1st catagory status ingesting'] = False
            logger.info(f"actual_result : {actual_result}")
            return actual_result
        else:
            actual_result['check 1st catagory status ingesting'] = True
            logger.info(f"work_flow_level : {work_flow_level}")
            # if work_flow_level == test_setting.work_flow_level['catagory_ingesting']:
            #     return actual_result
        insert_new_catagory_id = insert_new_catagory_record['id']

        upd_catagory_record = find_upd_catagory_record(req_get, insert_new_catagory_id)
        if test_type == "status_stric":
            # if work_flow_level >= test_setting.work_flow_level['catagory_complete']:
            #     if upd_catagory_record['status'] != "complete":
            #         logger.error("catagory not change to complete : upd_catagory_record : {upd_catagory_record}")
            #         actual_result[f'check catagory upd status complete'] = False
            #         return actual_result
            #     else:
            #         logger.info(f'catagory status change to : complete')
            #         actual_result[f'check catagory upd status complete'] = True
            #
            # elif work_flow_level == test_setting.work_flow_level['catagory_fail']:
            #     if upd_catagory_record['status'] != "failed":
            #         logger.error(f"catagory not change to fail")
            #         actual_result['check upd catagory status=failed'] = False
            #     else:
            #         actual_result['check upd catagory status=failed'] = True
            #         logger.info(f'status change to : failed')
            if upd_catagory_record['status'] != "complete":
                logger.error("catagory not change to complete : upd_catagory_record : {upd_catagory_record}")
                actual_result[f'check catagory upd status complete'] = False
                return actual_result

        else:
            if upd_catagory_record['status'] not in ["complete", "failed"]:
                logger.error(f"upd_catagory_record status wrong : upd_catagory_record : {upd_catagory_record}")
                actual_result['check upd catagory status'] = False
                return actual_result
            else:
                actual_result['check upd catagory status'] = True


        if upd_catagory_record['status'] != "complete":
            return actual_result


        if upd_catagory_record['status'] == "complete":
            if 'assets' not in upd_catagory_record:
                logger.info(f"assets not in catagory record upd_catagory_record: {upd_catagory_record}")
                actual_result['check complete_catagory_record assets'] = False
                return actual_result
            else:
                actual_result['check complete_catagory_record assets'] = True


        logger.info(f"upd_catagory_record : {upd_catagory_record}")
        check_asset_valid(upd_catagory_record['assets'])
        actual_result['check catagory assets valid'] = True

        insert_new_product_record = find_new_product_record(req_get, old_product_records)
        if insert_new_product_record[
                   'status'] != 'processing':
            logger.error(f"upload file, new record in product status not display processing : {insert_new_product_record}")
            actual_result['check 1st product status processing'] = False
            logger.info(f"actual_result : {actual_result}")
            return actual_result
        else:
            actual_result['check 1st product status processing'] = True
            logger.info(f"work_flow_level : {work_flow_level}")
            # if work_flow_level == test_setting.work_flow_level['product_processing']:
            #     return actual_result
        insert_new_product_id = insert_new_product_record['id']

        if insert_new_product_record['input_id'] != insert_new_catagory_id:
            logger.error(f"product input_id incorrect")
            logger.info(f"insert_new_product_record['input_id'] : {insert_new_product_record['input_id']}")
            logger.info(f"new catagory id : {insert_new_catagory_id}")
            actual_result['check product input_id'] = False
            return actual_result
        else:
            actual_result['check product input_id'] = True

        upd_product_record = find_upd_product_record(req_get, insert_new_product_id)
        if test_type == "status_stric":
            # if work_flow_level == test_setting.work_flow_level['product_complete']:
            #     if upd_product_record['status'] != "complete":
            #         logger.error("product not change to complete : upd_product_record : {upd_product_record}")
            #         actual_result[f'check product upd status complete'] = False
            #         return actual_result
            #     else:
            #         logger.info(f'product status change to : complete')
            #         actual_result[f'check product upd status complete'] = True
            #
            # elif work_flow_level == test_setting.work_flow_level['product_fail']:
            #     if upd_product_record['status'] != "failed":
            #         logger.error(f"product not change to fail")
            #         actual_result['check upd product status=failed'] = False
            #         return actual_result
            #     else:
            #         logger.info(f'product status change to : failed')
            #         actual_result['check upd product status=failed'] = True
            if upd_product_record['status'] != "complete":
                logger.error(f"product not change to complete : upd_product_record : {upd_product_record}")
                actual_result[f'check product upd status complete'] = False
                return actual_result

        else:
            if upd_product_record['status'] not in ["complete", "failed"]:
                logger.error(f"upd_product_record status wrong : upd_product_record : {upd_product_record}")
                actual_result['check upd product status'] = False
                return actual_result
            else:
                actual_result['check upd product status'] = True

        if upd_product_record['status'] != "complete":
            return actual_result


        if upd_product_record['status'] == "complete":
            if 'assets' not in upd_product_record:
                logger.info(f"assets not in product record upd_product_record: {upd_product_record}")
                actual_result['check upd_product_record assets'] = False
                return actual_result
            else:
                actual_result['check upd_product_record assets'] = True

        check_asset_valid(upd_product_record['assets'])
        actual_result['check product assets valid'] = True

        return actual_result

    except TimeoutError:
        logger.error("Error: Execution took too long")
        signal.alarm(0)
        actual_result['time during :'] = False
        return actual_result

    finally:
        signal.alarm(0)

    return  actual_result


@pytest.mark.functest
@pytest.mark.TEST00001
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path, new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00001: verify upload one file, if catagory record not insert new record, will fail", ["debug"], "shoes_001.txt", test_setting.upload_file_path,
         "abc_001.txt", test_setting.work_flow_level['catagory_not_new_record'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_catagory_not_new_record(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_catagory_not_new_record.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown

    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00002
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path, new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00002: verify upload one file, catagory is ingsting, test case will fail because status keep ingsting, not update", ["debug"], "shoes_001.txt", test_setting.upload_file_path, "abc_001.txt", test_setting.work_flow_level['catagory_ingesting'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_catagory_ingesting(
    suite_setupteardown,
    eachtest_setupteardown,
    dstring,
    old_file_name, old_file_path,
    new_file_name,
    work_flow_level, test_type,
    expected_result,
):
    test_upload_catagory_ingesting.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown
    
    actual_result = process_workflow(old_file_name, old_file_path,
    new_file_name,
    work_flow_level, test_type,
    req_get, req_upload
                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00003
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path, new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00003: verify upload one file, catagory is timeout, test case will fail", ["debug"], "shoes_001.txt",
         test_setting.upload_file_path, "abc_001.txt", test_setting.work_flow_level['catagory_upd_timeout'],
         "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_catagory_upd_timeout_unaccept(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_catagory_upd_timeout_unaccept.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown

    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result



@pytest.mark.functest
@pytest.mark.TEST00004
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path, new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00004: verify upload one file, if catagory upd status fail, test case will fail", ["debug"], "shoes_001.txt",
         test_setting.upload_file_path, "abc_001.txt", test_setting.work_flow_level['catagory_fail'],
         "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_catagory_fail_unaccept(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_catagory_fail_unaccept.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown

    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00005
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00005: verify upload one file, if generate catagory complete, not new product record, test case will fail", ["debug"], "shoes_001.txt", test_setting.upload_file_path, "abc_001.txt",
         test_setting.work_flow_level['catagory_complete_product_not_new_record'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_catagory_complete(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_catagory_complete.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown

    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00006
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00006: verify upload one file, if generate catagory complete, new product processing, because time out, fail", ["debug"], "shoes_001.txt", test_setting.upload_file_path, "abc_001.txt",
         test_setting.work_flow_level['product_processing'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_product_processing(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_product_processing.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00007
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00007: verify upload one file, if generate product timeout, test case will fail", ["debug"], "shoes_001.txt", test_setting.upload_file_path, "abc_001.txt",
         test_setting.work_flow_level['product_upd_timeout'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_product_upd_timeout(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_product_upd_timeout.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result

@pytest.mark.functest
@pytest.mark.TEST00008
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00008: verify upload one file, if product status fail, test case will fail", ["debug"], "shoes_001.txt", test_setting.upload_file_path, "abc_001.txt",
         test_setting.work_flow_level['product_fail'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_product_fail(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_product_fail.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result




@pytest.mark.priority_h
@pytest.mark.functest
@pytest.mark.systemtest
@pytest.mark.system_monitor_strict
@pytest.mark.TEST00101
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00101: upload one file, catagory and product generate complete, status=complete", ["debug"], "shoes_001.txt", test_setting.upload_file_path, "abc_001.txt",
         test_setting.work_flow_level['product_complete'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_product_complete(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_product_complete.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.priority_h
@pytest.mark.systemtest
@pytest.mark.system_monitor
@pytest.mark.TEST00201
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00201 : upload one file, monitor system, status is complete or fail", ["debug"], "shoes_001.txt", test_setting.upload_file_path, "abc_001.txt",
         test_setting.work_flow_level['product_complete'], "status_monitor", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_status_monitor(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_status_monitor.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


def process_db_monitor(req_get, test_type):
    actual_result = {}

    catagory_records = get_catagory_records(req_get)
    product_records = get_product_records(req_get)

    for catagory in catagory_records:
        if 'id' not in catagory.keys():
            logger.error(f"catagory record field missing : {catagory}")
            assert False
        if 'status' not in catagory.keys():
            logger.error(f"catagory record field missing : {catagory}")
            assert False

        if catagory['status'] == 'complete':
            if 'assets' not in catagory.keys():
                logger.error(f"catagory record field missing : {catagory}")
                assert False

            if len(catagory['assets']) == 0:
                logger.error(f"catagory assets is empty : {catagory}")
                assert False

        if test_type == "status_stric":
            if catagory['status'] not in ['ingesting', 'complete']:
                logger.error(f"catagory record field invalid status : {catagory}")
                assert False
        else:
            if catagory['status'] not in ['ingesting','complete', 'failed']:
                logger.error(f"catagory record field invalid status : {catagory}")
                assert False

    actual_result['catagory field check'] = True

    for product in product_records:
        if 'id' not in product.keys():
            logger.error(f"product new record field missing : {product}")
            assert False
        if 'input_id' not in product.keys():
            logger.error(f"product new record field missing : {product}")
            assert False
        if 'status' not in product.keys():
            logger.error(f"product new record field missing : {product}")
            assert False

        if product['status'] == 'complete':
            if 'assets' not in product.keys():
                logger.error(f"product new record field missing : {product}")
                assert False

            if len(product['assets']) == 0:
                logger.error(f"product assets is empty : {product}")
                assert False

        if test_type == "status_stric":
            if product['status'] not in ['processing', 'complete']:
                logger.error(f"product record field invalid status : {product}")
                assert False
        else:
            if product['status'] not in ['processing','complete', 'failed']:
                logger.error(f"product record field invalid status : {product}")
                assert False



    actual_result['product field check'] = True

    catagory_complete_id = set()
    for catagory in catagory_records:
        if catagory['status'] == 'complete':
            catagory_complete_id.add(catagory['id'])

    logger.info(f"all catagory_complete_id : {catagory_complete_id}")

    product_input_id = set()
    for product in product_records:
        product_input_id.add(product['input_id'])
    logger.info(f"all product_input_id : {product_input_id}")

    diff_id = catagory_complete_id ^ product_input_id
    if len(diff_id) != 0:
        logger.error(f"product_input_id different catagory_complete_id : diff_id : {diff_id}")
        assert False
    else:
        actual_result['pruduct input_id 1:1 complete catagory id'] = True

    logger.info(f"actual_result : {actual_result}")
    return actual_result

@pytest.mark.systemtest
@pytest.mark.dbtest
@pytest.mark.TEST00301
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, test_type, expected_result",
    [
        ("TEST00301: verify monitor system, all database status static, status fail not accept", ["debug"], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_monitor_db_status_stric(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        test_type,
        expected_result
):
    test_monitor_db_status_stric.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown

    actual_result = process_db_monitor(req_get, test_type)
    assert all(actual_result.values()) == expected_result

@pytest.mark.systemtest
@pytest.mark.dbtest
@pytest.mark.TEST00302
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, test_type, expected_result",
    [
        ("TEST00302: verify monitor system, all database status not static, status fail accept", ["debug"], "status_monitor", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_monitor_db_status_monitor(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        test_type,
        expected_result
):
    test_monitor_db_status_monitor.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown

    actual_result = process_db_monitor(req_get, test_type)
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00401
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00401 : upload file with invalid input path", ["debug"], "shoes_001.txt", '/wrong_path', "abc_001.txt",
         test_setting.work_flow_level['product_complete'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_invalid_file_path(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_invalid_file_path.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00402
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00402 : upload file with invalid input name", ["debug"], "shoes_009.txt", test_setting.upload_file_path, "abc_001.txt",
         test_setting.work_flow_level['product_complete'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_invalid_old_file_name(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_invalid_old_file_name.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result


@pytest.mark.functest
@pytest.mark.TEST00403
@pytest.mark.parametrize(
    "dstring, eachtest_setupteardown, old_file_name, old_file_path,new_file_name, work_flow_level, test_type, expected_result",
    [
        ("TEST00403 : upload file with invalid new file name", ["debug"], "shoes_001.txt", '/wrong_path', '',
         test_setting.work_flow_level['product_complete'], "status_stric", True),
    ],
    indirect=["eachtest_setupteardown"],
)
def test_upload_invalid_new_file_name(
        suite_setupteardown,
        eachtest_setupteardown,
        dstring,
        old_file_name, old_file_path,
        new_file_name,
        work_flow_level, test_type,
        expected_result,
):
    test_upload_invalid_new_file_name.__doc__ = dstring
    req_get, req_upload = eachtest_setupteardown


    actual_result = process_workflow(old_file_name, old_file_path,
                                     new_file_name,
                                     work_flow_level, test_type,
                                     req_get, req_upload
                                     )
    logger.info(f"actual_result : {actual_result}")
    assert all(actual_result.values()) == expected_result

