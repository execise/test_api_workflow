from tinydb import TinyDB, Query

db = TinyDB('file_db.json')

# Define a query to find the record to update
User = Query()
result = db.search(User.status == 'completed')

# If a matching record is found, update the value of the 'age' field
if result:
    db.update({'status': 'igs'}, User.file_name == 'shoes_upd.txt')
    print('Value updated')
else:
    print('No record found')
