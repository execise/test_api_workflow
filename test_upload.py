import requests
import os
# from utility import *
# import test_setting as test_setting

url = "http://localhost:5000/upload"

current_path = os.getcwd()

file_name = "pants_002.txt"

file_path = current_path + "/data/upload_files/" + file_name
print(f"file_path : {file_path}")

# Set the parameters for the file
files = {"file": open(file_path, "rb")}

upload_data = {}
upload_data["file_path"] = file_path
upload_data["file_name"] = file_name
# upload_data["work_flow_level"] = 1

print(f"upload_data : {upload_data}")
# Send the request to the Flask endpoint
response = requests.post(url, json=upload_data)

# Check the response status code
if response.status_code == 201:
    print("File uploaded successfully.")
else:
    print("Failed to upload file:", response.content)
