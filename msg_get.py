import sys

sys.path.append(".")



import requests
import json
import urllib3
import logging


import requests
import os
import test_setting as test_setting


logger = logging.getLogger("generation workflow test")

# local_url = "http://localhost:5000"
local_url = test_setting.local_url


current_path = os.getcwd()
data_path = current_path + "/data"
upload_file_path = data_path + "/upload_files"
test_dbs = data_path + "/test_dbs"

class msg_get_Error(Exception):
    """
        An exception class for msg_get
    """

class msg_get(object):
    """
     Webservice operation : GET, POST, DELETE, PUT, HEAD, PATCH
    """
    
    def __init__(self,local_url):
        self.url = local_url
    
    def opd_get(self,url_add = ''):
        logger.info(f'url_add : {url_add}')
        get_url = self.url + url_add
        logger.info(f"get_url : {get_url}")
        response = requests.get(get_url)
        return response
    
    def opd_get_catagory(self):
        return self.opd_get(url_add ='/input')


    def opd_get_product(self):
        return self.opd_get(url_add ='/product')

    def opd_post(self, url_add='', data=''):
        post_url = self.url + url_add
        logger.info(f'post_url : {post_url}')
        
        response = requests.post(post_url, json=data)
        return response


if __name__ == '__main__':
    test_url = local_url
    operation = msg_get(test_url)

    logger.info('---test get catagory: ')
    r = operation.opd_get_catagory()
    logger.info(f"catagory page output")
    logger.info(r.content)
    
    logger.info('---test get product: ')
    r = operation.opd_get_product()
    logger.info(f"product page output")
    logger.info(r.content)
    
