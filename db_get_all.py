from tinydb import TinyDB

file_db = TinyDB('file_db.json')

catagory_table = file_db.table('catagory_table')
product_table = file_db.table('product_table')


# Retrieve all records from the database
catagory_records = catagory_table.all()
product_records = product_table.all()


print(f"catagory_records : {catagory_records}")
print(f"product_records : {product_records}")
