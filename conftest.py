#!/usr/bin/python3
import sys

sys.path.append(".")

import logging
import pytest
import subprocess
import os
import signal
import multiprocessing
import time
from py.xml import html

from tinydb import TinyDB, Query

import test_setting as test_setting
from msg_get import *
from msg_upload import *


logger = logging.getLogger("generation workflow test")


def pytest_configure(config):
    config._metadata['test software version'] = '0.1'

@pytest.hookimpl(optionalhook=True)
def pytest_html_results_table_header(cells):
    cells.insert(1, html.th('Description'))
    cells.pop()

@pytest.hookimpl(optionalhook=True)
def pytest_html_results_table_row(report, cells):
    try:
        cells.insert(1, html.td(report.description))
    except AttributeError as error:
        logger.info("AttributeError trying to pytest_html_results_table_row - start")
        logger.error(error)
        logger.info("AttributeError trying to pytest_html_results_table_row - end")
    except Exception as e:
        logger.error(f"Unknown Exception trying to pytest_html_results_table_row : {e}")
    cells.pop()

@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    report.description = str(item.function.__doc__)

@pytest.fixture(scope="module")
def suite_setupteardown():
    logger.info("suite_setupteardown fixture start...")

    logger.info("software install ...")
    
    # # app_cmd = 'python3 app.py'
    # app_process = subprocess.Popen(['python3', 'app.py']
    #                                 # ,shell = True,
    #                                 # stdout = subprocess.PIPE,
    #                                 # stderr = subprocess.PIPE
    #                                 )
    # logger.info(f"app pid: {app_process.pid}")
    
    # time.sleep(1)

    # cmd_list_process = 'ps -A | grep app.py'
    # ps_value = os.popen(cmd_list_process).read()
    # logger.info(f"ps_value : {ps_value}")

    yield

    logger.info("software uninstall...")
    # os.kill(app_process.pid, signal.SIGTERM)
    
    logger.info("suite_setupteardown fixture end...")


@pytest.fixture
def eachtest_setupteardown(request):
    logger.info("eachtest_setupteardown fixture start...")


    web_server_flag = request.param[0]
    logger.info(f"web_server_flag : {web_server_flag}")

    logger.info("change system configuration...")
    if web_server_flag == 'debug':
        server_url = test_setting.local_url
    else:
        server_url = test_setting.prod_url

    logger.info(f'server_url : {server_url}')
    
    req_get = msg_get(server_url)
    req_upload = msg_upload(test_setting.local_url)
    
    # yield server_url
    yield req_get, req_upload

    logger.info("recover system configuration...")

    logger.info("eachtest_setupteardown fixture end...")
