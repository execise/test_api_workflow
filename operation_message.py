import sys

sys.path.append(".")



import requests
import json
import urllib3
import logging


import requests
import os
import test_setting as test_setting


logger = logging.getLogger("generation workflow test")

# local_url = "http://localhost:5000"
local_url = test_setting.local_url


current_path = os.getcwd()
data_path = current_path + "/data"
upload_file_path = data_path + "/upload_files"
test_dbs = data_path + "/test_dbs"

class operation_message_Error(Exception):
    """
        An exception class for operation_message
    """

class operation_message(object):
    """
     Webservice operation : GET, POST, DELETE, PUT, HEAD, PATCH
    """
    
    def __init__(self,local_url):
        self.url = local_url
    
    def opd_get(self,url_add = ''):
        logger.info(f'url_add : {url_add}')
        get_url = self.url + url_add
        logger.info(f"get_url : {get_url}")
        response = requests.get(get_url)
        return response
    
    def opd_get_catagory(self):
        return self.opd_get(url_add ='/input')


    def opd_get_product(self):
        return self.opd_get(url_add ='/product')

    def opd_post(self, url_add='', data=''):
        post_url = self.url + url_add
        logger.info(f'post_url : {post_url}')
        logger.info(f'data : {data}')

        
        response = requests.post(post_url, json=data)
        return response
        
    def opd_put(self, url_add='',data=''):
        logger.info(f'url_add : {url_add}')
        put_url = self.url + url_add
        logger.info(f"put_url : {put_url}")
        logger.info(f'data : {data}')

        response = requests.put(put_url, json=json.dumps(data))
        return response



if __name__ == '__main__':
    test_url = local_url
    operation = operation_message(test_url)

    # logger.info('---test get catagory: ')
    # r = operation.opd_get_catagory()
    # logger.info(f"catagory page output")
    # logger.info(r.content)
    #
    # logger.info('---test get product: ')
    # r = operation.opd_get_product()
    # logger.info(f"product page output")
    # logger.info(r.content)
    #
    #
    # logger.info('---test upload file: ')
    # file_name = test_setting.upload_file_name
    # file_path = test_setting.upload_file_path + '/' + file_name
    #
    # logger.info(f"file_path : {file_path}")
    # # files = {"file": open(file_path, "rb")}
    #
    # upload_data = {}
    # upload_data["file_path"] = file_path
    # upload_data["file_name"] = file_name
    #
    # logger.info(f"upload_data : {upload_data}")
    # # Send the request to the Flask endpoint
    # r = operation.opd_post(url_add='/upload', data=upload_data)
    #
    # # Check the response status code
    # if r.status_code == 201:
    #     logger.info("File uploaded successfully.")
    # else:
    #     logger.info("Failed to upload file:", r.content)
    #
    # logger.info(r.content)
    # logger.info(r.status_code)

    print('---test change: ')
    url_add = '/change'
    change_data = {}
    change_data["table"] = 'catagory'
    change_data['new_data'] = {'id':1010, 'status':'complete'}


    r = operation.opd_put(url_add, change_data)
    print(r.content)
    print(r.status_code)


